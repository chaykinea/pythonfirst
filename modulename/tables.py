import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt

T_base = np.loadtxt('Temperaturevalues.dat')
rho_base = np.loadtxt('Densityvalues.dat')

C_base = np.loadtxt('tableC.dat')
k_base = np.loadtxt('tablek.dat')
Q_base = np.loadtxt('tableQ.dat')

star_model = np.loadtxt('model1.dat', skiprows=1)

C = interpolate.interp2d(T_base, rho_base, C_base, kind='linear')
k = interpolate.interp2d(T_base, rho_base, k_base, kind='linear')
Q = interpolate.interp2d(T_base, rho_base, Q_base, kind='linear')
rho = interpolate.interp1d(star_model[:, 1] * 1e5, star_model[:, 3], kind='linear')
radii = interpolate.interp1d(star_model[:, 3], star_model[:, 1] * 1e5, kind='linear')

TiTe = np.loadtxt('ti-te.dat', skiprows=5)

T_e = interpolate.interp1d(np.power(10, TiTe[:, 0]), np.power(10, TiTe[:, 1]), kind='linear')


