#include <iostream>
#include <cmath>
#include <fstream>

using namespace std;

// main interpolation function( data for x direction, the length of x array, data for y direction, the length of y array,
// data for 2d function values [ table  n x l has to be reshaped into 1D array with length n*l ], x value for the point being interpolated, y value for the point being interpolated,
// extrapolation value in case we go out of boundary in any direction )
double interp2d(double data_x[],unsigned int len_x, double data_y[], unsigned int len_y, double data_z[], double x, double y, double extvalue);

// function interp_calc contains the law that is used for interpolation. Apart from point that need to be interpolated this function
// also takes four other points which are closest to the first one and surround it.
double interp_calc(double x, double y, double x_1, double x_2, double y_1, double y_2, double f_11, double f_21, double f_12, double f_22);

// this function finds  the closest coordinate [ x or y ] which is stored in initial data to the coodinate [ x or y ] of the point that is being interpolated
int interp_pointnumber(double points_data[],unsigned int right, unsigned int left, double point);

double interp_calc(double x, double y, double x_1, double x_2, double y_1, double y_2, double f_11, double f_21, double f_12, double f_22)
{
    return (f_11 * (x_2 - x) * (y_2 - y) + f_21 * (x - x_1) * (y_2 - y) + f_21 * (y - y_1) * (x_2 - x) + f_22 * (x - x_1) * (y - y_1))
           / ((x_2 - x_1) * (y_2 - y_1));
}


int interp_pointnumber(double points_data[], unsigned int right, unsigned int left, double point)
{
    double dist;
    unsigned int number;
    // this function divides data array into two parts [ they are equal or the difference in their lengths is 1 ] and it occurs over
    // and over again until there is one element left and its element is the closest to the given point coorinate

    if ((left + right)%2 != 0)
    {
        dist = abs(points_data[(left + right - 1) / 2] - point);
        number = (left + right - 1) / 2;
    }
    else
    {
        dist = abs(points_data[(left + right) / 2] - point);
        number = (left + right) / 2;
    }

    if (right != left)
    {
        if (dist > abs(points_data[number + 1] - point))
        {
            left = number + 1;
            interp_pointnumber(points_data, right, left, point);

        }
        else
        {
            right = number;
            interp_pointnumber(points_data, right, left, point);
        }
    }
    else
    {
        return right;
    }
}

double interp2d(double data_x[],unsigned int len_x, double data_y[], unsigned int len_y, double data_z[], double x, double y, double extvalue)
{
    int min_index;
    double f_11, f_22, f_12, f_21;
    double x_1, y_1, x_2, y_2;
    int x1, x2, y1, y2;

    if( x > data_x[len_x - 1] || y > data_y[len_y - 1] || x < data_x[0] || y < data_y[0])
    {
        // here extrapolation value comes into play if we happend to be outside array's boundary
        return extvalue;
    }
    else
    {
        min_index = interp_pointnumber(data_x, len_x, 0, x);

        if (data_x[min_index] - x > 0) {
            x_2 = data_x[min_index];
            x_1 = data_x[min_index - 1];

            x2 = min_index;
            x1 = min_index - 1;
        }
        else {
            x_1 = data_x[min_index];
            x_2 = data_x[min_index + 1];

            x1 = min_index;
            x2 = min_index + 1;
        }
        min_index = interp_pointnumber(data_y, len_y, 0, y);

        if (data_y[min_index] - y > 0) {
            y_2 = data_y[min_index];
            y_1 = data_y[min_index - 1];

            y2 = min_index;
            y1 = min_index - 1;
        }
        else {
            y_1 = data_y[min_index];
            y_2 = data_y[min_index + 1];

            y1 = min_index;
            y2 = min_index + 1;
        }


        f_11 = data_z[x1 + len_x * y1];
        f_22 = data_z[x2 + len_x * y2];
        f_12 = data_z[x1 + len_x * y2];
        f_21 = data_z[x2 + len_x * y1];

        return interp_calc(x, y, x_1, x_2, y_1, y_2, f_11, f_21, f_12, f_22);
    }

}


int main (void)
{


    unsigned int x_len = 650, y_len = 178, z_len = x_len * y_len;
    double x_data[x_len];
    double y_data[y_len];
    double z_data[z_len];
    double Ttest_[356], rhotest_[356], heatcaptest_[356];
    int i, j;

    FILE *f0, *f1, *f2, *f3, *f4;

    f1 = fopen( "file1.dat", "r"); // temperature = x
    f2 = fopen( "file2.dat", "r"); // density = y
    f3 = fopen( "file4.dat", "r"); // heat capacity = z(x,y)

    // important
    f4 = fopen( "test_data.dat", "r");  // this is used to test 2d interpolation function
    f0 = fopen( "out.dat", "w");  // this is used to test 2d interpolation function


    for (i = 0; i < 356; i++)
    {
        fscanf(f4, "%le %le %le", &rhotest_[i], &Ttest_[i],  &heatcaptest_[i]); // this is data for test
    }
    // important

    cout << 1;


    for (i = 0; i < x_len; i++)
    {
        fscanf(f1, "%le", &x_data[i]); // temperature
    }

    for (i = 0; i < y_len; i++)
    {
       fscanf(f2, "%le", &y_data[i]); // density
    }


    for (j = 0; j < y_len; j++)
    {
        for (i = 0; i < x_len; i++)
        {
            fscanf(f3, "%le", &z_data[i+x_len*j]);  //heat capacity
        }
    }



    for (i = 0; i < 356; i++)
    {
        fprintf(f0, "%15.8e %15.8e %15.8e\n", Ttest_[i], rhotest_[i], interp2d(x_data,x_len, y_data, y_len, z_data, Ttest_[i], rhotest_[i], 1)); //test
    }



}


