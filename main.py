import numpy as np
import matplotlib.pyplot as plt
import loaddata
import constants

T_0 = 1e10
dt = 10
t = 0

Nzones = 30 # number of spherical zones that neutron star will be divided into

T = T_0 * np.ones(Nzones) # initial temperature of neutron star

def C(a, b): # heat capacity
    return loaddata.C(a, b)
def k(a, b): # thermal conductivity
    return loaddata.k(a, b)
def Q(a, b): # neutrino emissivity
    return loaddata.Q(a, b)
def T_e(a):  # surface temperature
    return loaddata.T_e(a)
def Phi(a):  # dimensionless gravitational potential
    return loaddata.Phi(a)
def rho(a):  # density
    return loaddata.rho(a)



Rho = np.logspace(14.69, 10, Nzones + 1)
R_boundaries = loaddata.radii(Rho)

r = (R_boundaries[1:] + R_boundaries[:-1])
r = r/2

r_top = np.concatenate([r[1:], [R_boundaries[-1]]])
r_bot = np.concatenate([[R_boundaries[0]], r[:-1]])

T_top = np.concatenate([T[1:], [T[-1]]])
T_bot = np.concatenate([[T[0]], T[:-1]])

def timederivative(r, r_bot, r_top, T, T_top, T_bot):  # time derivative of temperature including GR effects

    x_0 = np.ones(Nzones)
    x = np.ones(Nzones)
    y = np.ones(Nzones)
    z = np.ones(Nzones)

    for i in range(0, Nzones):

        if i == Nzones-1: # for outer layer

            z[i] = Q(T[i], Rho[i]) / C(T[i], rho(r[i])) * np.exp(Phi(r[i])) # relates to neutrino emission

            p = constants.sigma * (T_e(T[i]) ** 4) * np.exp( Phi(r[i])) / C(T[i], rho(r[i])) / (r_top[i]-r_bot[i]) # photon radiation

            x_0[i] = relativity_sqrt(r[i]) * np.exp(-Phi(r[i])) * (x[i] + y[i]) / (C(T[i], rho(r[i])) * (r_top[i]-r_bot[i])) - z[i] - p # altogether

        else: # for every layer except for the outer one

            x[i] = ((r_top[i] / r[i]) ** 2) * (T_top[i] * np.exp(Phi(r_top[i])) - T[i] * np.exp(Phi(r[i]))) / (r_top[i] - r[i]) # relates to top layer
            y[i] = ((r_bot[i] / r[i]) ** 2) * (T_bot[i] * np.exp(Phi(r_bot[i])) - T[i] * np.exp(Phi(r[i]))) / (r[i] - r_bot[i]) # relates to bottom layer

            x[i] = x[i] * np.exp(Phi(r_top[i])) * k(T_top[i], rho(r_top[i])) * relativity_sqrt(r_top[i]) # relates to top layer
            y[i] = y[i] * np.exp(Phi(r_bot[i])) * k(T_bot[i], rho(r_bot[i])) * relativity_sqrt(r_bot[i]) # relates to bottom layer

            z[i] = Q(T[i], Rho[i]) / C(T[i], rho(r[i])) * np.exp(Phi(r[i])) # relates to neutrino emission

            x_0[i] = relativity_sqrt(r[i]) * np.exp(-Phi(r[i])) * (x[i] + y[i]) / (C(T[i], rho(r[i])) * (r_top[i]-r_bot[i])) - z[i] # altogether

    return x_0

def timederivative2(r, r_bot, r_top, T, T_top, T_bot): #  time derivative of temperature without any GR effects

    x_0 = np.ones(Nzones)
    x = np.ones(Nzones)
    y = np.ones(Nzones)
    z = np.ones(Nzones)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
    for i in range(0, Nzones):

        if i == Nzones-1: # for outer layer

            x[i] = ((r_top[i] / r[i]) ** 2) * (T_top[i] - T[i]) / (r_top[i] - r[i]) # relates to top layer
            y[i] = ((r_bot[i] / r[i]) ** 2) * (T_bot[i] - T[i]) / (r[i] - r_bot[i]) # relates to bottom layer

            x[i] = x[i] * k(T_top[i], rho(r_top[i]))  # relates to top layer
            y[i] = y[i] * k(T_bot[i], rho(r_bot[i]))  # relates to bottom layer

            z[i] = Q(T[i], Rho[i]) / C(T[i], rho(r[i])) # relates to neutrino emission

            p = constants.sigma * (T_e(T[i]) ** 4) / C(T[i], rho(r[i])) / (r_top[i]-r_bot[i]) # photon radiation

            x_0[i] = (x[i] + y[i]) / (C(T[i], rho(r[i])) * (r_top[i]-r_bot[i])) - z[i] - p # altogether

        else: # for every layer except for the outer one

            x[i] = ((r_top[i] / r[i]) ** 2) * (T_top[i] - T[i]) / (r_top[i] - r[i]) # relates to top layer
            y[i] = ((r_bot[i] / r[i]) ** 2) * (T_bot[i] - T[i]) / (r[i] - r_bot[i]) # relates to bottom layer

            x[i] = x[i] * k(T_top[i], rho(r_top[i]))  # relates to top layer
            y[i] = y[i] * k(T_bot[i], rho(r_bot[i]))  # relates to bottom layer

            z[i] = Q(T[i], Rho[i]) / C(T[i], rho(r[i]))  # relates to neutrino emission

            x_0[i] =  (x[i] + y[i]) / (C(T[i], rho(r[i])) *(r_top[i]-r_bot[i])) - z[i] # altogether

    return x_0


def relativity_sqrt(r): # relativistic square

    return np.sqrt(1 - 2 * constants.G * loaddata.mass(r) / (( constants.c ** 2 ) * r))

n = 1
j = 1

while j < 15:

    T += timederivative(r, r_bot, r_top, T, T_top, T_bot) * dt

    T_top = np.concatenate([T[1:], [T[-1]]])
    T_bot = np.concatenate([[T[0]], T[:-1]])

    if n < j: # this condition is responsible for time step

        n += 1

        if j == 2:
            dt = 20
        elif j == 3:
            dt = 50
        elif j == 4:
            dt = 100
        elif j == 5:
            dt = 1000
        elif j == 6:
            dt = 10000
        elif j == 7:
            dt = 70000

    if t >= constants.yrtosec * loaddata.timesteps[j] and j == int(loaddata.stepnumbers[j]):
        print('current time = ' + str(t))

        if j > 0: # I use this condition when I want to choose certain graphs from the total amount

            plt.figure(j)
            plt.figure(j).patch.set_facecolor('white')
            plt.xlabel('Density, g/cm^3')
            plt.ylabel('Temperature, K')
            plt.yscale('log')
            plt.xscale('log')
            plt.title('Time = ' + str(loaddata.timesteps[j]) + ' yr')
            plt.plot(rho(r), T, 'r-', label='using python')
            plt.plot(loaddata.Density_1, loaddata.Temperature[:, j-1], 'b-', label='using c')
            plt.legend(loc=4)
            plt.show(j)
            plt.savefig('data.pdf')

        j += 1

    t += dt










