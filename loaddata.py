import numpy as np
from scipy import interpolate
import constants
import matplotlib.pyplot as plt

T_base = np.loadtxt('Temperaturevalues.dat')
rho_base = np.loadtxt('Densityvalues.dat')

T_base2 = np.loadtxt('Temperaturevalues2.dat')
rho_base2 = np.loadtxt('Densityvalues2.dat')

C_base = np.loadtxt('tableC.dat')
k_base = np.loadtxt('tablekappa.dat')
Q_base = np.loadtxt('tableQ.dat')
k_base2 = np.loadtxt('tablek.dat')

star_model = np.loadtxt('model.dat', skiprows=1)

C = interpolate.interp2d(T_base, rho_base, C_base, kind='linear')
k = interpolate.interp2d(T_base, rho_base, k_base, kind='linear')
Q = interpolate.interp2d(T_base, rho_base, Q_base, kind='linear')
rho = interpolate.interp1d(star_model[:, 1] * 1e5, star_model[:, 3], kind='linear')

radii = interpolate.interp1d(star_model[:, 3], star_model[:, 1] * 1e5, kind='linear')
mass = interpolate.interp1d(star_model[:, 1] * 1e5, star_model[:, 0] * constants.Sun_m, kind='linear')
Phi = interpolate.interp1d(star_model[:, 1] * 1e5, star_model[:, 4], kind='linear')

TiTe = np.loadtxt('ti-te.dat', skiprows=5)

T_e = interpolate.interp1d(np.power(10, TiTe[:, 0]), np.power(10, TiTe[:, 1]), kind='linear')

timesteps = np.array([0, 1.250061e-05, 2.500121e-05, 1e-4, 1e-3, 1e-2, 1e-1, 1, 2, 5, 10, 20, 50, 100, 1000])
stepnumbers = np.arange(15)

data_1 = np.loadtxt('config1.dat',   skiprows=4)
[Density_1,   Temperature_1,  D_1,  Heat_capacity_1,  Radiation_1,  G_1,  T_1] = np.hsplit(data_1,   7) # time = 1.250061e-05 yr

data_2 = np.loadtxt('config2.dat',   skiprows=4)
[Density_2,   Temperature_2,  D_2,  Heat_capacity_2,  Radiation_2,  G_2,  T_2] = np.hsplit(data_2,   7) # time = 2.500121e-05 yr

data_3 = np.loadtxt('config3.dat',   skiprows=4)
[Density_3,   Temperature_3,  D_3,  Heat_capacity_3,  Radiation_3,  G_3,  T_3] = np.hsplit(data_3,   7) # time = 1e-4 yr

data_4 = np.loadtxt('config4.dat',   skiprows=4)
[Density_4,   Temperature_4,  D_4,  Heat_capacity_4,  Radiation_4,  G_4,  T_4] = np.hsplit(data_4,   7) # time = 1e-3 yr

data_5 = np.loadtxt('config5.dat',   skiprows=4)
[Density_5,   Temperature_5,  D_5,  Heat_capacity_5,  Radiation_5,  G_5,  T_5] = np.hsplit(data_5,   7) # time = 1e-2 yr

data_6 = np.loadtxt('config6.dat',   skiprows=4)
[Density_6,   Temperature_6,  D_6,  Heat_capacity_6,  Radiation_6,  G_6,  T_6] = np.hsplit(data_6,   7) # time = 1e-1 yr

data_7 = np.loadtxt('config7.dat',   skiprows=4)
[Density_7,   Temperature_7,  D_7,  Heat_capacity_7,  Radiation_7,  G_7,  T_7] = np.hsplit(data_7,   7) # time = 1 yr

data_8 = np.loadtxt('config13.dat',   skiprows=4)
[Density_8,   Temperature_8,  D_8,  Heat_capacity_8,  Radiation_8,  G_8,  T_8] = np.hsplit(data_8,   7) # time = 2 yr

data_9 = np.loadtxt('config13.dat',   skiprows=4)
[Density_9,   Temperature_9,  D_9,  Heat_capacity_9,  Radiation_9,  G_9,  T_9] = np.hsplit(data_9,   7) # time = 5 yr

data_10 = np.loadtxt('config10.dat',   skiprows=4)
[Density_10,   Temperature_10,  D_10,  Heat_capacity_10,  Radiation_10,  G_10,  T_10] = np.hsplit(data_10,   7) # time = 10 yr

data_11 = np.loadtxt('config11.dat',   skiprows=4)
[Density_11,   Temperature_11,  D_11,  Heat_capacity_11,  Radiation_11,  G_11,  T_11] = np.hsplit(data_11,   7)  # time = 20 yr

data_12 = np.loadtxt('config12.dat',   skiprows=4)
[Density_12,   Temperature_12,  D_12,  Heat_capacity_12,  Radiation_12,  G_12,  T_12] = np.hsplit(data_12,   7) # time = 50 yr

data_13 = np.loadtxt('config13.dat',   skiprows=4)
[Density_13,   Temperature_13,  D_13,  Heat_capacity_13,  Radiation_13,  G_13,  T_13] = np.hsplit(data_13,   7) # time = 100 yr

data_14 = np.loadtxt('config14.dat',   skiprows=4)
[Density_14,   Temperature_14,  D_14,  Heat_capacity_14,  Radiation_14,  G_14,  T_14] = np.hsplit(data_14,   7) # time = 1000 yr

Temperature = np.hstack([Temperature_1, Temperature_2, Temperature_3, Temperature_4, Temperature_5, Temperature_6, Temperature_7,
                        Temperature_8, Temperature_9, Temperature_10, Temperature_11, Temperature_12, Temperature_13, Temperature_14])


